module.exports = {
    placeholder() {
        return 'Loading products...';
    },
    data(req) {
        return new Promise(resolve => {
            resolve({
                data: {
                    name: 'A Book',
                    price: '2.41 $'
                }
            });
        });
    },
    content(data) {
        return {
            main: `<div class="product"><div>${data?.name}</div><small>${data?.price}</small></div>`
        };
    }
};